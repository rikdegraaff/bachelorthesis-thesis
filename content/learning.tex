\section{Learning Heuristics}

To quantify the value of extensions to the concept language and feature space bring, we will learn several generalized potential heuristics with the features that we expressed with them, and analyze their performance.
Each of the approaches takes as inputs a set of alive states from a common domain $\mathcal{S}$ and a set of hand-crafted \first{candidate features} $\mathcal{F}$ of the same domain.
We will find heuristics $h : \mathcal{S} \rightarrow \mathds{R}$ by solving some mixed integer program with variables $W = \{ w_f \in \mathds{R} \mid f \in \mathcal{F} \}$ and consider $h(s)$ to equal $\sum_{f \in \mathcal{F}} w_f \cdot f(s)$.
The expression $f(s)$ here represents the value of the feature $f$ in the state $s$.
For our first approach, we follow \cite{frances2019generalized} and compute the simplest heuristic which is descending and dead-end avoiding on $\mathcal{S}$.
We say a heuristic $h$ is descending on a state $s$ if $s$ has at least one successor with a heuristic value that is less than $h(s) - 1$.
We say a heuristic $h$ is dead-end avoiding on a state $s$ if every unsolvable successor of $s$ has a heuristic value greater than or equal to $h(s)$.
A descending and dead-end avoiding heuristic on a set of states $\mathcal{S}$ is a heuristic which is descending and dead-end avoiding on every solvable, non-goal state in $\mathcal{S}$.
A heuristic with this property guides most standard greedy algorithms directly to a goal.
The mixed integer program for this heuristic is a program $\Mstrict(\mathcal{S}, \mathcal{F})$ with $w_f \in \mathds{R}$ they defined as
\begin{align*}
\min_W &\sum_{f \in \mathcal{F}} [w_f \neq 0] \mathcal{K}(f)  &\text{ subject to }\\
       &\bigvee_{\pr{s} \in \suc(s)} h(s) \geq h(\pr{s}) + 1 &\text{ for all } s \in \mathcal{S}\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ unsolvable}}} h(\pr{s}) \geq h(s) &\text{ for all } s \in \mathcal{S}.
\end{align*}
It is worth noting that neither this mixed integer program nor any of the other two are linear.
However, all three can be solved by modern MIP solvers after some rewriting as described by \cite{frances2019generalizedtech}.
In broad terms, it requires introducing indicator constraints for disjunctions and two binary variables for the indicator function in the objective.
\par
Since we do not expect to find a descending and dead-end avoiding heuristic for more complex domains like Sokoban, we also introduce a version of the above linear program with slack variables.
This mixed integer program $\Mslack(\mathcal{S}, \mathcal{F})$ with variables $w_f \in \mathds{R}$ and additional slack variables $u_s, v_{s,\pr{s}} \in \mathds{R}$ is defined as
\begin{align*}
\min_{W,u,v} &\sum u_{s,\pr{s}} + \sum v_{s,\pr{s}} &\text{ subject to }\\
       &\bigvee_{\pr{s} \in \suc(s)} h(s) + u_s \geq h(\pr{s}) + 1 &\text{ for all } s \in \mathcal{S}\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ unsolvable}}} h(\pr{s}) + v_{s,\pr{s}} \geq h(s) &\text{ for all } s \in \mathcal{S}\\
       &u_s \geq 0 \ \ \ \text{ for all } s \in \mathcal{S}\\
       &v_{s,\pr{s}} \geq 0 \ \ \ \text{ for all } s \in \mathcal{S}, \pr{s} \in \suc(s), \pr{s} \text{ unsolvable}.
\end{align*}
Note here that we no longer take the complexity of a feature into account and simply compute the generalized potential heuristic which is closest to being descending and dead-end avoiding using all the features provided.
The reasoning behind this is that, since the features are hand-crafted by a domain expert, we expect most of them to be useful.
Provided the set of states is sufficiently large and varied, a feature $f$ which is not informative will essentially act as noise and will not be useful to minimize the slack variables.
We can, therefore, expect $w_f$ to be close to 0 and the domain expert can choose to discard it at their discretion to make the heuristic more computationally efficient and, hopefully, generalize better.
\par
The final heuristic aims to locally approximate the perfect heuristic $\hstar$.
We obtain this heuristic by solving the mixed integer program $\Mstar(\mathcal{S}, \mathcal{F})$ with $w_f \in \mathds{R}$ and additional slack variables $u_{s,\pr{s}}, v_{s,\pr{s}} \in \mathds{R}$
\begin{align*}
\min_{W,u,v} &\sum |u_{s,\pr{s}}| + \sum v_{s,\pr{s}} &\text{ subject to }\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ solvable}}} h(s) - h(\pr{s}) + u_{s,\pr{s}} = \hstar(s) - \hstar(\pr{s}) &\text{ for all } s \in \mathcal{S}\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ unsolvable}}} h(\pr{s}) + v_{s,\pr{s}} \geq \max_{\substack{t \in \suc(s) \cup \{s\}\\ t \text{ solvable}}} h(t) &\text{ for all } s \in \mathcal{S}\\
       &v_{s,\pr{s}} \geq 0 \ \ \ \text{ for all } s \in \mathcal{S}, \pr{s} \in \suc(s), \pr{s} \text{ unsolvable}.
\end{align*}
The first type of constraint encapsulates how well the heuristic approximates the perfect heuristic locally.
A heuristic $h$ which fulfills these constraints with all $u_{s,\pr{s}} = 0$ can only differ the perfect heuristic by a constant on all states in $\mathcal{S}$ and their solvable successors.
The second type of constraint deals with unsolvable successors of solvable states.
Since the value of the features, and by extension of the heuristic, can only be a finite number, the heuristic can not indicate an unsolvable state by having an infinite value.
Instead, we strengthen the requirement from $\Mslack(\mathcal{S}, \mathcal{F})$ and compare the value of the heuristic for the unsolvable state to that for its predecessor and all of its solvable successors.
The same considerations about generalization and computational efficiency we made for $\Mslack(\mathcal{S}, \mathcal{F})$ apply here too.
\par
The mixed integer program $\Mstar(\mathcal{S}, \mathcal{F})$ with $w_f \in \mathds{R}$ can be equivalently expressed as the purely linear program
\begin{align*}
\min_{W,u,v} &\sum_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ solvable}}} u_{s,\pr{s}} + \sum v_{s,\pr{s}} &\text{ subject to }\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ solvable}}} u_{s,\pr{s}} \geq h(s) - \hstar(s) - h(\pr{s}) + \hstar(\pr{s}) &\text{ for all } s \in \mathcal{S}\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ solvable}}} u_{s,\pr{s}} \geq h(\pr{s}) - \hstar(\pr{s}) - h(s) + \hstar(s) &\text{ for all } s \in \mathcal{S}\\
       &\bigwedge_{\substack{\pr{s} \in \suc(s),\\ \pr{s} \text{ unsolvable}}} \bigwedge_{\substack{t \in \suc(s) \cup \{s\}\\ t \text{ solvable}}} v_{s,\pr{s}} \geq h(t) - h(\pr{s}) &\text{ for all } s \in \mathcal{S}\\
       &u_{s,\pr{s}} \geq 0 \ \ \ \text{ for all } s \in \mathcal{S}, \pr{s} \in \suc(s), \pr{s} \text{ solvable}\\
       &v_{s,\pr{s}} \geq 0 \ \ \ \text{ for all } s \in \mathcal{S}, \pr{s} \in \suc(s), \pr{s} \text{ unsolvable}.
\end{align*}

\subsection{Reporting Difficult States}

When solving $\Mslack(\mathcal{S}, \mathcal{F})$ and $\Mstar(\mathcal{S}, \mathcal{F})$, we can identify states which the heuristic captures badly by considering the slack variables with the highest value.
We can automatically present the domain expert with these states and their successors as well as the feature values and total heuristic value for these states.
These can serve as a basis for the expert to identify which important aspects of the domain are not adequately expressed by the features they specified.
They can then extend $\mathcal{F}$ with features they expect might alleviate the problem.
\par
What results from these basic elements is an iterative process. 
The domain expert starts out by expressing some features $\mathcal{F}$ of the domain they expect are significant.
They can then solve one or both of the mixed integer programs $\Mslack(\mathcal{S}, \mathcal{F})$ and $\Mstar(\mathcal{S}, \mathcal{F})$ with $\mathcal{S}$ being the alive states of a few small tasks of the domain.
After refining $\mathcal{F}$ as described above, the expert can solve the mixed integer programs again with the new set of features.
When the resulting heuristic captures everything in the sample states $\mathcal{S}$ reasonably well, the expert can add additional tasks and thereby expand $\mathcal{S}$.
These steps can be repeated until the expert is satisfied with the resulting heuristic or improving upon it becomes infeasible due to the size of the mixed integer program.
\par
There are two logical improvements to this process which we do not explore in this thesis.
The first is the ability to generate constraints incrementally, sampling states from $\mathcal{S}$ randomly and adding more as needed.
However, we do give the domain expert the ability to manually specify a sample rate that determines what portion of states from state spaces are added into $\mathcal{S}$.
The second is the capacity to improve the heuristic using data from states seen during searches with the heuristic.
\cite{frances2019generalized} introduced approaches to incorporate these improvements into the calculation of a descending and dead-end avoiding heuristic in their setting where the feature pool is automatically generated and can grow unboundedly.
These approaches do not translate easily to a setting where the objective is to minimize slack variables and features are handcrafted.
We, therefore, consider these possible improvements to be the subject of future work.