\section{Introduction}
The oft-repeated maxim ``physics, not advice'' \citep{mcdermott20001998}, states that a specification of a planning domain should simply describe the physical properties of the domain and refrain from encoding advice to the planner on how to solve tasks in the domain.
This is sensible because the person specifying the domain might have incorrect ideas about the best solution strategies.
It is a separation of concerns. The specification of domain and planning are different problems and should not be intertwined.
However, the ultimate goal of planning should arguably be to help solve real-world problems.
They require close to no domain knowledge to be applied to a domain.
All that is required, is that the problem is formulated in a format the planning system can understand.
A*\citep{hart1968formal} and greedy best-first search in combination with an informative domain-independent heuristic are considered the state of the art in optimal and suboptimal classical planning respectively.
But these approaches to problem-solving mostly can not compete with domain-specific algorithms precisely because of their generality.
In this thesis we want to take a step towards finding a golden middle way between classical planning and domain-specific solutions.
\par
To do so we consider adding advice for the planner back in, but as a step separate from the specification of the domain.
In doing so, we will be straying very close to generalized planning, where domains of planning tasks with a common underlying structure are automatically analyzed and the results are used to solve individual tasks within that domain much more efficiently.
\cite{frances2019generalized} presented one such attempt. They introduced an automated way to discover an informative heuristic\footnote{To be precise, the heuristics are guaranteed to be descending and dead-end avoiding \citep{seipp2016correlation}.} in the form of a weighted sum of features expressed in a concept language strictly less powerful than first-order logic.
They found that these features often represent aspects that have a clear meaning to humans which makes them supremely suited a language for humans to give advice to planning systems.
A weakness of this approach is that the expressiveness of the logic as well as how the features can be combined to form a heuristic must be kept at a minimum to prevent a blow-up of the search space of possible heuristics.
This resulted in an inability to express a useful heuristic for certain domains.
\par
In this thesis, we will explore how useful concept languages are as a means to express expert knowledge of a domain.
We will consider a standard concept language and a few extensions to this concept language, a few different features based on these languages, as well as different ways of combining these features into the final heuristic and attempt to identify the advantages and downsides of each extension.
\par
We start by formalizing our setting by defining planning tasks, concept languages, how these two can be combined, and what sort of features we want to be able to express.
We then describe a workflow that aides a domain expert with specifying informative features and automatically learns a heuristic based on these features.
On the practical side, we implement this workflow as a tool and provide a few notes on the nuts and bolts.
Finally, we present some experimental results based on our attempts to use the tool ourselves interspersed with fictitious dialogues between two people learning to use the tool as a way to illustrate the capabilities and peculiarities of this workflow.
