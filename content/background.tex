\section{Background}

This section will formalize classical planning and the concept language we will be using throughout the paper.
We will then clarify how these two elements can be combined.
Finally, we will define the heuristics that are ultimately the subject of this thesis.

\subsection{Classical Planning}

In this thesis, we will consider planning tasks which can be represented as a tuple 
$\langle \mathcal{P}, \mathcal{A}, \mathcal{C}, \mathcal{O}, \mathcal{I}, \mathcal{G} \rangle$.
$\mathcal{P}$ is a set of predicates. $\mathcal{A}$ is a set of action schemas.
$\mathcal{C}$ is a set of constants.
If $a \in \mathcal{A}$ then $\para(a)$ is a set of parameters, $\pre(a)$ any logical formula over the predicates in $\mathcal{P}$ and their negations applied to $\para(a) \cup \mathcal{C}$, and $\eff(a)$ a conjunction of a subset of the same predicates and their negations.
$\cost(a) \in \Rpos$ is called the cost of an action schema.
$\mathcal{O}$ is a set of objects.
$\mathcal{I}$ is called the initial state and is a full assignment of truth values to all predicates in $\mathcal{P}$ applied to $\mathcal{C} \cup \mathcal{O}$.
$\mathcal{G}$ is the goal condition and is a consistent conjunction of a subset of the same predicates and their negations applied to $\mathcal{C} \cup \mathcal{O}$.
$\mathcal{A}$ and $\mathcal{O}$ together induce the set of ground actions of the task.
This set is obtained by replacing the parameters $\para(a)$ in $\pre(a)$ and $\eff(a)$ of action schemas $a$ by all possible combinations of objects from $\mathcal{O}$.
The cost of a ground action is the same as that of its action schema.
We say two planning tasks are part of the same domain if they share $\mathcal{P}$,  $\mathcal{A}$, and  $\mathcal{C}$.
In this case we call $\langle \mathcal{P}, \mathcal{A}, \mathcal{C} \rangle$ the domain of the planning tasks.
\par
Many classical planning tasks expressed in PDDL \citep{mcdermott1998pddl} fit our definition quite naturally.
We consider predicates and types declared in a PDDL domain file to be the predicates of the task.
Likewise, the action schemas in the domain file are the action schemas in the planning task.
Objects defined in the domain file are considered constants, whereas those defined in the task file make up the objects.
The initial state and goal as defined in PDDL are also simply the initial state and goal condition of the task.
PDDL contains some features which do not map cleanly onto our definition of planning tasks, but these can be compiled away.
\par
A planning task induces a state space $\langle S, L, T, s_I, S_G \rangle$.
Every state $s \in S$ represents a unique assignment of truth values to all predicates in $\mathcal{P}$ applied to $\mathcal{C} \cup \mathcal{O}$.
We call this assignment (or interpretation) $I(s)$ and write $I(s) \vDash \varphi$ if a formula $\varphi$ holds under the interpretation $I(s)$.
Every label $l \in L$ corresponds to a ground action induced by $\mathcal{A}$ and $\mathcal{O}$.
We will call this ground action $\act(l)$.
$T \subseteq S \times L \times S$ is the set of transitions such that
$\langle s, l, \pr{s} \rangle \in T$ if and only if $I(s) \vDash \pre(\act(l))$ and $I(\pr{s}) \vDash \eff(\act(l))$ and the set of predicates $I(s)$ and $I(\pr{s})$ assign different truth values to is minimal.
$s_I \in S$ such that $\mathcal{I} = I(s_I)$ is the initial state.
$S_G \subseteq S$ consists of all states $s$ such that $I(s) \vDash \mathcal{G}$.
\par
The successors $\suc(s)$ of a state $s$ are all states $\pr{s}$ such that $\langle s, l, \pr{s} \rangle \in T$ for any $l \in L$.
A path between two states $s_0$ and $s_n$ is a sequence $\langle l_1, \dots, l_n \rangle$ such that there exists a sequence of states $\langle s_1, \dots , s_{n-1} \rangle$ with $\langle s_{i-1}, l_i, s_i \rangle \in T$ for all $i \in \{1, \dots n\}$.
The cost of a path $\cost(\langle l_1, \dots, l_n \rangle)$ is $\sum_{i=1}^{n} \cost(\act(l_i))$.
An optimal path between two states is a path with minimal cost.
A plan is a path from the initial state to any goal state.
An optimal plan is a plan with minimal cost.
\first{Reachable} states are states to which a path from the initial state exists and \first{solvable} states are states from which a path to any goal state exists.
We call a state \first{alive} if it is both solvable and reachable, but not a goal state.
\par
A heuristic for a planning task is a function $h: S \rightarrow \mathds{R}$.
The perfect heuristic $\hstar$ is the heuristic which is equal to the cost of an optimal path between the state and any goal state.
A generalized heuristic is a function that is defined for all states of all tasks in a given domain.


\subsection{Concept Languages}

\first{Concept languages} or \first{description logics} are a family of logic-based representation languages, most of which are more expressive than propositional logic but still decidable \citep{baader2007description}.
Concept languages deal with \first{individuals}; classes of individuals which are called \first{concepts} and can be thought of as properties; and binary relationships between individuals which are called \first{roles}.
Members of the family of concept languages differentiate from one another by which \first{constructors} for concepts and roles they allow.
We will consider various concept languages which will all be supersets of the standard language $\mathcal{SOI}$ with the added constructor role-value-map.

\minisection{Syntax}
Concepts and roles are defined inductively based on finite sets of named concepts, roles, and individuals.
Top $\top$ and bottom $\bot$ are both concepts.
Any \first{named concept} is a concept and any \first{named role} is a role.
If $a_1, \dots, a_n$ are \first{named individuals}, $C$ and $\pr{C}$ are concepts, and $R$ and $\pr{R}$ are roles, then the following holds:
the \first{negation} or \first{complement} of a concept $\neg C$,
the \first{union} $C \sqcup \pr{C}$ and the \first{intersection} $C \sqcap \pr{C}$ of two concepts,
the \first{existential restriction} $\exists R.C$ and the \first{universal restriction} $\forall R.C$ of a concept by a role,
the \first{nominal concept} $\{a_1, \dots, a_n\}$,
and the \first{role-value-map} $R=\pr{R}$ are all concepts.
The \first{inverse} of a role $R^{-1}$, the \first{transitive closure} of a role $R^{+}$ and the \first{composition} of two roles $R \circ \pr{R}$ are also roles.

\minisection{Semantics}
The semantics of this description logic are given by a \first{universe} $\Delta$ and a \first{model} $.\model{}$.
This model maps every individual $a$ to an object $a\model{} \in \Delta$, every named concept $C$ to a set of objects $C\model{} \subseteq \Delta$, and every named role $R$ to a relation between objects $R\model{} \subseteq \Delta^2$.
The interpretation of all other concepts and roles is defined recursively as follows.
As before, $a_1, \dots, a_n$ are named individuals, $C$ and $\pr{C}$ are concepts, and $R$ and $\pr{R}$ are roles.

\begin{gather*}
\top\model{} = \Delta, 
\bot\model{} = \emptyset, 
(\neg C)\model{} = \Delta \backslash C\model{}, \\
(C \sqcup \pr{C})\model{} = C\model{} \cup \pr{C}\model{},
(C \sqcap \pr{C})\model{} = C\model{} \cap \pr{C}\model{}, \\
(\exists R.C)\model{} = \{ x \mid \exists y : (x,y) \in R\model{} \wedge y \in C\model{} \}, \\
(\forall R.C)\model{} = \{ x \mid \forall y : (x,y) \in R\model{} \rightarrow y \in C\model{} \}, \\
\{a_1, \dots, a_n\}\model{} = \{a_1\model{}, .., a_n\model{}\}, \\
(R = \pr{R})\model{} = \{ x \mid (x,y) \in R\model{} \leftrightarrow (x,y) \in \pr{R}\model{} \}, \\
(R^{-1})\model{} = \{ (x,y) \mid (y,x) \in R\model{} \}, \\
\begin{align*}
{R^{+}}\model{} = \{ (x_0,x_n) &\mid \exists x_1, \dots x_{n-1} \\
                               &: (x_{i-1}, x_i) \in R\model{} \text{ for all } i \in \{ 1, \dots, n \} \},
\end{align*} \\
(R \circ \pr{R})\model{} = \{ (x,y) \mid \exists z : (x,z) \in R\model{} \wedge (z,y) \in \pr{R}\model{} \}.
\end{gather*}

\minisection{Extensions}
We also consider the following extensions to this base concept language.
\par
\begin{definition}
For all comparison operators $\sim \in \{ =, >, <, \geq, \leq \}$ we define \first{qualified cardinality restrictions} introduced by \cite{hollunder1991qualifying}.
Let $R$ be a role and $C$ a concept, the interpretation of the qualified cardinality restriction $\sim n\ R.C$ is
\begin{align*}
(\sim n\ R.C)\model{} = \{ x \mid \#\{ y \mid (x, y) \in R\model{} \wedge y \in C\model{} \} \sim n \}.
\end{align*}
\end{definition}
This extension of description logics is commonly referred to as $\mathcal{Q}$.
\par
We further introduce a way to deal with $n$-ary relations with $n > 2$.
To this end we introduce \first{$n$-ary roles}.
When a role has an arity $n$ which is higher than two, we will make this explicit by indicating this with a subscript, for instance: $R_n$.
We extend $.\model{}$ such that $R_n\model{} \subseteq \Delta^n$.
In order to make $n$-ary roles work with the concept language we will define a constructor which makes it possible to reduce the arity of a role.
To make this useful, this constructor will be a combination of a selection and a projection.
\begin{definition}
Let $R_n$ be an $n$-ary role, $m \leq n$ a natural number, and $C$ a concept.
The interpretation of the \first{atomic selection} of $R_n$, ${R_n}^{m \in C}$ is
\begin{gather*}
({R_n}^{m \in C})\model{} = \{ (x_1, \dots, x_n) \mid x_m \in C\model{} \}.
\end{gather*}
\end{definition}
\begin{definition}
Let $R_n$ be an $n$-ary role, and $m \leq n$ a natural number.
The interpretation of the \first{projection} of $R_n$, ${R_n}^{\bar{\pi}(m)}$ is
\begin{gather*}
({R_n}^{\bar{\pi}(m)})\model{} = \{ (x_1, \dots, x_{m-1}, x_{m+1}, \dots, x_n) \mid \exists x_m. (x_1, ..., x_n) \in R_n\model{} \}.
\end{gather*}
\end{definition}
Using these two definitions, we can now define the \first{partial restriction} of an $n$-ary role $R_n[a_1, \dots, a_n]$, where $a_1, \dots, a_n$ are either individuals or concepts.
For each $a_i$, if $a_i$ is a concept, then we simply apply the selection $.^{i \in a_i}$ to the role, if it is an individual, we apply both the selection $.^{i \in \{ a_i \}}$ as well as the projection $.^{\bar{\pi}(i)}$.
For example, if $x$ is an individual and $C$ a concept, the complex role $R_3[\top, x, C]$ would expand to $(({R_3}^{2 \in \{ x \}})^{3 \in C})^{\bar{\pi}(2)}$.
It should be noted that the projection and selection together form a generalization of the existential restriction, but that the partial restriction does not.
\par
We also introduce the \first{universal quantifier} and the \first{existential quantifier} as
\begin{align*}
(\forall a \in C.R_n)\model{} = \bigcap_{x \in C\model{}} R_n(a)\model{a=x},\\
(\exists a \in C.R_n)\model{} = \bigcup_{x \in C\model{}} R_n(a)\model{a=x}.
\end{align*}
In these definitions, $.\model{a=x}$ denotes the model which is identical to $.\model{}$ with the addition of the new named individual $a$ such that $a\model{} = x$.
These quantifiers are particularly useful when used in combination with the partial restriction.
The introduction of the universal and existential quantifier and the partial restriction make the concept language much more expressive, at the cost of a lot of nice computational guarantees which make concept languages attractive in the first place.
However, we will only be concerned with evaluating concepts and roles under a given interpretation, so this is of no importance.
\par
Finally, we introduce \first{role intersection} and \first{role union} which are defined as
\begin{align*}
    (R \sqcap \pr{R})\model{} = R\model{} \cap \pr{R}\model{},\\
    (R \sqcup \pr{R})\model{} = R\model{} \cup \pr{R}\model{}.
\end{align*}

\subsection{Concept Languages of Planning Tasks}

We consider planning tasks represented in PDDL.
We consider objects and constants -- objects which are defined in the domain file -- to be individuals,
unary predicates to be named concepts, binary predicates to be named roles, and $n$-ary predicates to be $n$-ary roles is $n>2$.
We consider the universe to be constant across all states and to be identical to the set of individuals.
Every state $s$ of the state space induced by the planning task defines a model $.\model{s}$.
$a\model{s} = a$ for all named individuals $a$ and states $s$.
For every named concept $C$, $C\model{s} = \{ x \mid I(s) \vDash C(x) \}$,
and for every named role $R$, $R\model{s} = \{ (x, y) \mid I(s) \vDash R(x,y) \}$.
\par
We also introduce a \first{goal constructor} $._G$ which can be applied only to named roles and concepts with the following semantics.
If $C$ is a named concept and $R$ is a named role and $s$ is a state in a state space with a set of goal states $S_G$ then
\begin{gather*}
(C_G)\model{s} = \bigcap_{\pr{s} \in S_G} C\model{\pr{s}}, \\
(R_G)\model{s} = \bigcap_{\pr{s} \in S_G} R\model{\pr{s}}.
\end{gather*}
The concept language of a planning domain $\langle \mathcal{P}, \mathcal{A}, \mathcal{C} \rangle$ is the language which has the concepts and roles derived from the predicates in $\mathcal{P}$ as its named concepts and roles as well as the individuals derived from the constants in $\mathcal{C}$ as its named individuals.
We also introduce a named concept for each nullary predicate that have the interpretation $\Delta$ in states where the predicates hold and $\emptyset$ in states where they do not.

\subsection{Generalized Potential Heuristics}

\cite{frances2019generalized} introduced \first{generalized potential heuristics} as a weighted sum over features which map states to integers.
\begin{definition}
Let $S$ be a set of states which are not necessarily part of the same state space and $\mathcal{F}$ a set of features $f : S \rightarrow \mathds{Z}$.
Let $w : \mathcal{F} \rightarrow \mathds{R}$ be a weight function mapping features to weights.
The value of the potential heuristic with features $\mathcal{F}$ and weights $w$ on a state $s \in S$ is
$$ h(s) = \sum_{f \in \mathcal{F}} w(f) \cdot f(s).$$ 
\end{definition}
In this thesis we will consider generalized potential heuristics which are well-defined on all states $s$ that are part of any state space induced by any planning task belonging to the same domain.
This requires that all the features must be well-defined on the same states.
To this end, we will express features in terms of concepts and roles in the concept language of the domain.
We call these features the features of this planning domain.
As a starting point, we use the same features \cite{bonet2019learning} introduced.
\begin{definition}
Let $C$ and $\pr{C}$ be concepts and $R$ a role in the concept language of a planning domain.
The value of the \first{cardinality feature} $|C|$ in the state $s$ is $|C\model{s}|$.
The value of the \first{minimal distance feature} $\dist(C, R, \pr{C})$ in $s$ is $\min n \text{ such that } \exists x_0, \dots x_n \text{ with } x_0 \in C\model{s}, x_n \in \pr{C}\model{s} \text{ and } (x_{i-1},x_i) \in R\model{s} \text{ for all } i \in \{ 1, \dots , n \}$.
If such a sequence of individuals $x_0, \dots x_n$ does not exist, we define the value to be $0$.
\end{definition}
\minisection{Extensions}
We will also consider the following extensions to the set of possible features.
\begin{definition}
Let $f_1$ and $f_2$ be features of a planning domain.
The value of the \first{product feature} $f_1 \cdot f_2$ in the state $s$ is $f_1(s) \cdot f_2(s)$.
\end{definition}
%\begin{definition}
%Let $C$ and $\pr{C}$ be concepts, and $R$ a role in the concept language of a planning domain.
%The value of the \first{maximal distance feature} $\maxdist(C, R, \pr{C})$ in the state $s$ is 
%$$\max_{x, \pr{x}} dist(\{x\}, R, \{\pr{x}\}) \text{ with } x \in C\model{s}  \text{ and } \pr{x} \in \pr{C}\model{s}.$$
%\end{definition}
%\begin{definition}
%Let $C$ and $\pr{C}$ be concepts, and $R$ and $R_c$ roles in the concept language of a planning domain.
%The value of the \first{minimal cost feature} $\mincost(C, R, \pr{C}, R_c)$ in the state $s$ is
%\begin{align*}
%\min_{x_0, \dots, x_n} \sum_{i=1}^n \dist(\{x_{i-1}\}, R_c, \{x_{i}\}) &\text{ with } x_0 \in C\model{s} \text{ and } x_n \in \pr{C}\model{s} \\
%                                                                       &\text{ and } (x_{i-1}, x_i) \in R\model{s} \text{ for all } i \in {1, \dots, n}.
%\end{align*}
%\end{definition}
\begin{definition}
Let $h$ be a generalized heuristic of the domain $\mathcal{D}$. The value of the \first{heuristic feature} $f_h$ in the state $s$ in the domain $\mathcal{D}$ is $h(s)$. 
\end{definition}
By allowing any generalized heuristic of the domain as a feature, we allow a set of features to contain both human intuition about a domain and information state of the art heuristics are good at detecting.

\minisection{Complexity}
If $X$ is a feature, role or concept consider its \first{complexity} $\mathcal{K}(X)$ to be the size of its syntax tree.
For example, $\mathcal{K}(\dist(C, R, \pr{C})) = 1 + \mathcal{K}(C) + \mathcal{K}(R) + \mathcal{K}(\pr{C})$.
The complexity of $\top$ and $\bot$ is 0, the complexity of all heuristic features, named concepts, named roles, and nominal concepts is 1.
This definition differs slightly from the one introduced by \cite{frances2019generalized}.
They defined the complexity of a feature simply as the sum of the complexities of all the involved concepts and roles.
We deviate from their definition because we introduced features which are defined inductively.
